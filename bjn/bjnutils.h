#ifndef _BJNUTILS_H_
#define _BJNUTILS_H_

#include "bjn.h"

#ifdef __cplusplus
extern "C" {
#endif

 file_offset filelen(FILE *f);

 int finde_func_counts_and_names(FILE *fout, LPCHAR funcName, file_offset *pfunc, file_offset *plast, int *i_of_func, LPCHAR *fnames);
 int update_func_block_NEXT_FUNC_OFFSET(FILE *fout, file_offset pbase, file_offset value);
 int update_func_block_DATA_BLOCK_OFFSET(FILE *fout, file_offset pbase, file_offset value);
 int update_func_block_FMIN(FILE *fout, file_offset pbase, float value);
 int update_func_block_FMAX(FILE *fout, file_offset pbase, float value);
 int update_func_name(FILE *fout, file_offset pbase, LPCHAR name);
 int get_func_block_DATA_BLOCK_OFFSET(FILE *fout, file_offset pbase, file_offset *value);

int update_datablock_DATA_BLOCK_OFFSET(FILE *fout, file_offset pbase, file_offset value);
int get_data_block_DATA_BLOCK_OFFSET(FILE *fout, file_offset pbase, file_offset *value);
int get_data_block_BORDERS(FILE *fout, file_offset pbase, big_size *i0, big_size *j0, big_size *k0,	
						   big_size *in, big_size *jn, big_size *kn);

int get_data_block_BIT_REDUCED(FILE *fout, file_offset pbase, int * nBitsReduce);

int put_char (FILE *file, char a);
int get_char (FILE *file, char *a);
int get_longL(FILE *fout, LPINT pn);

int get_func_description(FILE *fout, LPFUNC_HEADER pfh);
int put_func_description(FILE *fout, FUNC_HEADER fh);
int get_N1N2N3(FILE *fout, int *in, int *jn, int *kn);

int put_block_of_float_mas(FILE *fout, LPLPLPFLOAT f, int nBitsReduce,
						   int i0, int j0, int k0, int ni, int nj, int nk);


int update_data_block_BORDERS(FILE *fout, file_offset pbase, 
						   big_size i0, big_size j0, big_size k0,	
						   big_size in, big_size jn, big_size kn,
						   int nBitsReduce);

int get_block_of_float_mas(FILE *fout, LPLPLPFLOAT f, 
						   int i0g, int j0g, int k0g, 
						   int n1,int n2,int n3, 
						   int *pnBitsReduce,
						   int i0, int j0, int k0, 
						   int ni, int nj, int nk);

int get_data_block_BIT_REDUCED(FILE *fout, file_offset pbase, int *nBitsReduce);

void prn_stat(LPCHAR pref,int ret, int ni, int nj, int nk, int n_s, int n_r);

int put_long(FILE *file, long a);
int put_int(FILE *file, int a);
int put_float(FILE *file, float a);
int put_string(FILE *fout, LPCHAR name);
int put_file_offset(FILE *file, file_offset a);
int put_big_size(FILE *file, big_size a);

int get_char(FILE *fin, LPCHAR c);
int get_long(FILE *fin, uLong *a);
int get_int(FILE *fin, int *a);
int get_big_size(FILE *fin, big_size *a);
int get_file_offset(FILE *fin, file_offset *a);
int get_float(FILE *file, float *a);
int get_string(FILE *fout, LPCHAR *pname);

int put_float_mas(FILE *fout, LPLPLPFLOAT f, int n1,int n2,int n3, int nBitsReduce);

int put_compressed_block(FILE *f, LPCHAR buf, int n);
int get_compressed_block(FILE *f, LPLPCHAR buf, LPINT n);

int rebyte_long_mas(LPCHAR tar, int n_tar, LPCHAR sou, int n);
int rebyte_float_mas(LPCHAR tar, int n_tar, LPCHAR sou, int n);
int unrebyte_float_mas(LPCHAR tar, int n_tar, LPCHAR sou, int n);

uLong set_bits_mask(int nBitsReduce);

void prn_func_description(FUNC_HEADER fh);
void prn_bjnh(BJN_HEADER bjnh);

#define prpr_d(a,b) printf(#a "." #b "=%d\n",a.b);
#define prpr_s(a,b) printf(#a "." #b "=`%s`\n",a.b);

#define prpr_d1(a,b) printf( #b "=%d\n",a.b);
#define prpr_s1(a,b) printf( #b "=`%s`\n",a.b);

//================================================

int IsBlocksCrossed( int i0, int j0, int k0, int ni, int nj, int nk,
					 int i0g,int j0g,int k0g,int ing,int jng,int kng);

int	ReCopyBlock(
	LPLPLPFLOAT f_source, int i0, int j0, int k0, int ni, int nj, int nk,
	LPLPLPFLOAT f_target, int i0g,int j0g,int k0g,int ing,int jng,int kng);

//----------------------------------------------

//int ReadBjnGzippedScalar8RecHead(LPCHAR target, LPINT pn1, LPINT pn2, LPINT pn3, LPBJN_HEADER pbjnh);

//----------------------------------------------

#ifdef __cplusplus
}
#endif

#endif /* _BJNUTILS_H_ */
