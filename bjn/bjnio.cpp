#include "bjn.h"
#include "string.h"

int finde_func(FILE *fout, LPCHAR funcName, file_offset *pfunc, file_offset *plast);

/* WriteBjnGzippedScalar8RecInit c������� ����� ��� ������ �������. */
int WriteBjnGzippedScalar8RecInit(
								  LPCHAR target, // ��� �����
								  LPCHAR grid_name, // ��� ����� ����� ��� ""
								  big_size n1, big_size n2, big_size n3 // ������� ���������� �����
								  )
{
/*
����������: 
���
n1 n2 n3
������ ����� ����� � ��� �����
����� ����� ������� 1			{update}
*/
 FILE *fout;
 size_t  bytes;

 if( (fout=fopen(target,"wb")) == NULL )
   { printf("\n Can't create file %s ",target ); return 125; }

 bytes=fwrite((LPCHAR)LABELScalar8RecGzip03,LLABEL,1,fout);

 if(bytes != 1) { printf("\n Can't write file %s\n",target ); return 126; }

 if(put_file_offset(fout, 0)) return 127;	// ���� ��� �� ����� �������
 if(put_big_size(fout, n1)) return 128;
 if(put_big_size(fout, n2)) return 129;
 if(put_big_size(fout, n3)) return 130;
 if(put_string(fout, grid_name)) return 131;

 if(fclose(fout))return 132;

 return 0;
}

int write_func_header(FILE *fout, LPCHAR funcName)
{ 
 int err;
 file_offset func_position,pcurrent,plast;

 func_position=filelen(fout); // ������ ����� �����

 if(file_seek(fout,LLABEL,SEEK_SET))return 134; // ��������� ���

 // �������� ������� �������
 if(get_file_offset(fout, &pcurrent)) return 135;

 if(!pcurrent)
 {   // ��� �� ����� �������
	 pcurrent=LLABEL; // ��������� ��������� �� ������ �������
 }
 else
 {
	 /*
	 ����� ��������� ���� �������� �������
	 �������� � ��� ��������� ������ �� ����� �����
	 */

	 if(err=finde_func(fout, funcName, &pcurrent, &plast))return err; // ��� ��������� ����, ���� ��������� �� ������ ��������������
     if(!pcurrent)
		 pcurrent=plast;
 }

 if(file_seek(fout,pcurrent,SEEK_SET))return 136; // ������� ��������� �� ����������� ���� �������� �������
 if(put_big_size(fout, func_position))return 137; // ������� ��������� ������� ����� �������� �������
		 
// �������� ���� �������� �������

 if(file_seek(fout,func_position,SEEK_SET)) return 138; // �������� � ������� ������ ����� �������

 /*
<���� ������� i>
	����� ����� ������� (i+1)	{update}
	����� ����� ������ i.1		{update}
	fmin fmax					{update}
	������ ����� �������
	��� �������
 */

 if(put_big_size(fout, 0)){ printf("139 01"); return 139;} // ��� ���������� ����� �������� �������
 if(put_big_size(fout, 0)) return 140; // ���� ��� ������ ������
 if(put_float(fout,  1.)) return 141; // ��� min : min>max !
 if(put_float(fout, -1.)) return 142; // ��� max: min>max !
 if(put_string(fout, funcName)) return 143; // 
		 
	return 0;
}

int finde_first_func_header(FILE *fout, LPCHAR *name, file_offset *pcurrent, file_offset *pnext)
{
 *pnext=(file_offset)0;

 if(file_seek(fout,LLABEL,SEEK_SET)) return 116; // ��������� ���
 if(get_file_offset(fout, pcurrent))	 return 155; 

 if(!(*pcurrent))
 {   // ��� �� ����� �������
	 return 0;
 }

 if(file_seek(fout,*pcurrent, SEEK_SET)) return 144; // �������� � ����� �������� �������

 if(get_file_offset(fout, pnext)) return 145;

//	����� ����� ������� (i+1)	{update}
//	����� ����� ������ i.1		{update}
//	fmin fmax					{update}
 if(file_seek(fout, *pcurrent +2*sizeof_file_offset +2*sizeof_float, SEEK_SET)) return 146; // �������� � ���� ����� �������
 if(get_string(fout,name)) return 147;

 return 0;
}

int finde_next_func_header(FILE *fout, LPCHAR *name, file_offset pcurrent, file_offset *pnext)
{
 *pnext=0;
 if(!pcurrent)
	 return 0;

 if(file_seek(fout,pcurrent,SEEK_SET)) return 148; // �������� � ������� �������
 if(get_file_offset(fout, pnext)) return 149;

 // �������� � ���� ����� �������

 if(file_seek(fout, pcurrent +2*sizeof_file_offset +2*sizeof_float, SEEK_SET)) return 150; // �������� � ���� ����� �������
 if(get_string(fout,name)) return 151;

 return 0;
}

int finde_func(FILE *fout, LPCHAR funcName, file_offset *pfunc, file_offset *plast)
{
 int n_funcs;

 return finde_func_counts_and_names(fout, funcName, pfunc, plast, &n_funcs,NULL);
}

int finde_func_counts_and_names(FILE *fout, LPCHAR funcName, file_offset *pfunc, file_offset *plast, int *i_of_func, LPCHAR *fnames)
{
	// ������������ �������� �� ������ ���������� � ������� funcname
	// ������������ ����� ��� ������� �������� �������
	// ���� ������� �� �������, �� ������������ ����� �������� �������
	// plast �������� �������� ���������� ����� �������� �������

 file_offset pcurrent,pnext;
 char *name=NULL;  // !!mberr �������� ������ ������ ��-�� �� ������������ name
 int err;

 if(i_of_func) (*i_of_func)=0;

 if(err=finde_first_func_header(fout,&name,&pcurrent,&pnext))
 { if(name) free(name);
		return err;	 }
 *pfunc=pcurrent;
 *plast=pcurrent;
 if((fnames)&&(name))
	str_alloc_cpy(fnames[(*i_of_func)],name);


 if(pcurrent)
 do
 {
	 if(!strcmp(name,funcName))
		 break;

	*plast=pcurrent;
	 pcurrent=pnext;
	 if(name) free(name);
	 name=NULL;
	 if(err=finde_next_func_header(fout,&name,pcurrent,&pnext))
	 { if(name) free(name);
		return err; }

	 if(i_of_func) 
	 {
		 (*i_of_func)++;
		 if((fnames)&&(name))
			str_alloc_cpy(fnames[(*i_of_func)],name);
	 }
 }	 
 while( pcurrent );

 if(name) free(name);

*pfunc=pcurrent;
 return 0;
}

int	WriteBjnGzippedScalar8RecFuncMinMax(
								  LPCHAR target,
								  LPCHAR funcName, 
								  float fmin, 
								  float fmax)
{
 FILE *fout;
 file_offset pfunc,plast;
 int err;

 if( (fout=fopen(target,"r+b")) == NULL )
   { printf("\n Can't open file %s ",target ); return 109;}

 if(err=finde_func(fout,funcName,&pfunc,&plast))return err;
 if(!pfunc)
	 {
		// ����� ������� ���� ���, ���� �������� ���� �������� �������
		if(err=write_func_header(fout,funcName))return err;
		if(err=finde_func(fout,funcName,&pfunc,&plast))return err;
	 }

 if(!pfunc){ printf("can't finde just written func\n"); return 200; }

 if(file_seek(fout,pfunc+2*sizeof_file_offset,SEEK_SET))return 112;

 if(put_float(fout, fmin)) return 110;
 if(put_float(fout, fmax)) return 111;

 fclose(fout);
	return 0;
}

/* WriteBjnGzippedScalar8RecFunc ������ � ���� ��������� ������� */
int WriteBjnGzippedScalar8RecFunc(
                  LPCHAR target, // ��� �����
				  LPCHAR funcName, // ��� �������
				  LPLPLPFLOAT f, // ���������� ������ ������������ ������� 
				  big_size n1, big_size n2, big_size n3, // ������� ������� f
				  int nBitsReduce) // ����� �� ����������� ��� ��������
{
 return WriteBjnGzippedScalar8RecFuncByBlock
		( target, funcName, f,
		  0,0,0,
		  n1,n2,n3,
		  nBitsReduce
		);
}

/* TODO ���������� �������� ���������� ������� WriteBjnGzippedScalar8RecFuncByBlock */
int	WriteBjnGzippedScalar8RecFuncByBlock(
								  LPCHAR target,
								  LPCHAR funcName, 
								  LPLPLPFLOAT f,
								  big_size i0g, big_size j0g, big_size k0g,
								  big_size ing, big_size jng, big_size kng,
								  int nBitsReduce)
{ 
 /* TODO
 �������� ������� ��� ��������
 ��������� ��������
 int   n1,n2,n3;
 n1=n2=n3=0;
 */

 //==================

/*
 ��������� ������� �������
 ���� ������� ����
 {
	��������� ����������� ������������ ��������� � ����������� �������
 }
 �������� ����
 */


 FILE *fout;
 file_offset pfunc,plast,pdatablock,pnewdatablock,plastdatablock;
 int err;

 if( (fout=fopen(target,"r+b")) == NULL )
   { printf("\n Can't open file %s ",target ); return 113;}

 if(err=finde_func(fout,funcName,&pfunc,&plast))return err;
 if(!pfunc)
	 {
		// ����� ������� ���� ���, ���� �������� ���� �������� �������
		if(err=write_func_header(fout,funcName)) return err;
		if(err=finde_func(fout,funcName,&pfunc,&plast))return err;
	 }

 if(!pfunc) { printf("\n Error: WriteBjnGzippedScalar8RecFuncByBlock: Can't finde just written func\n"); return 199; }

 // pfunc - ����� ����� �������� �������

//========================================
 
 pnewdatablock=filelen(fout); // ����� �� �������� ������� ������ ���� ������
 Dprintf("\npfunc=%d pnewdatablock=%u for storing\n",pfunc,pnewdatablock);


 if(err=get_func_block_DATA_BLOCK_OFFSET(fout, pfunc, &pdatablock)) return err;// ������� ����� ����� ������

 if(!pdatablock) // ���� ��� ���
 {
	// ���� ������ �������� ��������� � ���� �������� �������
	if(err=update_func_block_DATA_BLOCK_OFFSET(fout,pfunc,pnewdatablock)) return err;
 }
 else // ���� ����� ������ ����
 {
	// ���� ����� ��������� ���� � ���������, ��� ��� ����������� � ����������

	// �������� ����������� ��� ���������� ������ � �����������

	while(pdatablock)
		 {
			// �������� ����������� � ��������� ������
			big_size i0, j0, k0,  in, jn, kn;

			if(err=get_data_block_BORDERS(fout,pdatablock, &i0, &j0, &k0,	&in, &jn, &kn)) return err+194000;

			if(IsBlocksCrossed(  i0,  j0,  k0,	 in,  jn,  kn,
								i0g, j0g, k0g,	ing, jng, kng))
			{
				printf("Error: WriteBjnGzippedScalar8RecFuncByBlock: Part of block is always present\n");
				return 114;
			}
			
			plastdatablock=pdatablock;
			if(err=get_data_block_DATA_BLOCK_OFFSET(fout,plastdatablock,&pdatablock)) return err;
		 }

	if(err=update_datablock_DATA_BLOCK_OFFSET(fout,plastdatablock,pnewdatablock)) return err;
 }

 // ������ ���� �� ������ pnewdatablock �������� ����� ���� ������

 {
//	 printf("pnewdatablock=%u\n",pnewdatablock);

 if(err=update_datablock_DATA_BLOCK_OFFSET(fout, pnewdatablock, 0)) return err+107000;
 if(err=update_data_block_BORDERS(fout, pnewdatablock,  i0g, j0g, k0g,	ing, jng, kng, nBitsReduce)) return err+156000;
 if(err=put_block_of_float_mas(fout, f, nBitsReduce, i0g, j0g, k0g,	ing, jng, kng)) return err+108000;

//  pnewdatablock=filelen(fout); // ����� �� �������� ������� ������ ���� ������
//	 printf("pnewdatablock=%u after storing\n",pnewdatablock);

 }

 fclose(fout);

 return 0;
}


int	 ReadBjnGzippedScalar8RecFuncByBlock(
								  LPCHAR source, 
								  LPCHAR funcName, 
								  LPLPLPFLOAT *pf, // ������ �� ���������� ������ ������� f
								  big_size i0g, big_size j0g, big_size k0g,
								  big_size ing, big_size jng, big_size kng) // ��� ����� ��������� �������
{ 
 FILE *fout;
 file_offset pfunc,plast,pdatablock,plastdatablock;
 big_size nall=ing*jng*kng; // ������� ��������� ���� ��������
 int err;

 if(BjnDummy)
 {
	 big_size i,j,k;

	 printf("BjnDummy ReadBjnGzippedScalar8RecFuncByBlock03\n");

	 *pf=alloc_float_mas_n1_n2_n3(ing, jng, kng); if(!(*pf))return 119;

	 for(i=0;i<ing;i++)
	 for(j=0;j<jng;j++)
	 for(k=0;k<kng;k++)
		 (*pf)[i][j][k]=(float)(BjnFunc);

	 return 0;
 }

 *pf=NULL;

 if( (fout=fopen(source,"rb")) == NULL )
   { printf("\n Can't open file %s ",source ); return 115;}

 if(err=finde_func(fout,funcName,&pfunc,&plast))return err;
 if(!pfunc)
	 {
		// ����� ������� ���
		 return 116;
	 }

 // pfunc - ����� ����� �������� �������

//========================================
 
 if(get_func_block_DATA_BLOCK_OFFSET(fout, pfunc, &pdatablock))return 117;// ������� ����� ����� ������

 if(!pdatablock) // ���� ��� ���
 {
	// ������ ������ ���
	 return 118;
 }

 // ���� ����� �����, �������������� � �����

 *pf=alloc_float_mas_n1_n2_n3(ing, jng, kng); if(!(*pf))return 119;

 while(pdatablock)
 {
	// �������� ����������� � ��������� ������
	big_size i0, j0, k0,  in, jn, kn;

	if(err=get_data_block_BORDERS(fout,pdatablock, &i0, &j0, &k0,	&in, &jn, &kn))return err+120000;

	if(IsBlocksCrossed(  i0,  j0,  k0,	 in,  jn,  kn,
						i0g, j0g, k0g,	ing, jng, kng))
	{
		int nBitsReduce;
		if(err=get_data_block_BIT_REDUCED(fout,pdatablock, &nBitsReduce))return err+121000;

		Dprintf("Blocks are crossed\n");
		// ���� ������� ������ ��������

		if(err=get_block_of_float_mas(fout, *pf, i0g, j0g, k0g, ing, jng, kng, &nBitsReduce, i0, j0, k0,	in, jn, kn))return err+122000;

		// � ������� �� �� �������� ?!

		nall-=
			(minab(i0+in,i0g+ing)-maxab(i0,i0g))*
			(minab(j0+jn,j0g+jng)-maxab(j0,j0g))*
			(minab(k0+kn,k0g+kng)-maxab(k0,k0g));
	}

	plastdatablock=pdatablock;
	if(err=get_data_block_DATA_BLOCK_OFFSET(fout,plastdatablock,&pdatablock))return err+123000;
 }

 fclose(fout);

 if(nall)
	 return 124;

 return 0;
}

int ReadBjnGzippedScalar8RecFunc( LPCHAR source, LPCHAR funcName, 
								  LPLPLPFLOAT *pf, //	������ �� ���������� ������ ������� f
								  LPINT pn1, LPINT pn2, LPINT pn3)
{  
 FILE *fout;
 int err;

 *pf=NULL;

 if( (fout=fopen(source,"rb")) == NULL )
   { printf("\n Can't open file %s ",source ); return 166;} 

 if(get_N1N2N3(fout, pn1, pn2, pn3)) return 171;

 fclose(fout);

 err=ReadBjnGzippedScalar8RecFuncByBlock(source, funcName, pf,
		 						  0, 0, 0, 	  *pn1, *pn2, *pn3);
 if(err)
	printf("After ReadBjnGzippedScalar8RecFuncByBlock in ReadBjnGzippedScalar8RecHead: err=%d\n",err);

 return err;
}

