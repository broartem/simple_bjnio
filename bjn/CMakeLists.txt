cmake_minimum_required (VERSION 2.8) 


set (BJN_PROJECT BJN)
set (ZLIB_LIBRARIES ZLIB)
add_subdirectory (Zlibfiles/)


project (${BJN_PROJECT})

set (BJN_HEADERS 		
		bjn.h
		bjndebug.h
		bjnio.h
		bjnmemory.h
		bjntypes.h
		bjnutils.h)
set (BJN_SOURCES 
		 
		bjncompressing.cpp
		bjnerrmes.cpp
		bjninfo.cpp
		bjnio.cpp
		bjnioD.cpp
		bjnmemory.cpp
		bjnupdate.cpp
		bjnutils.cpp
		bjnwrapper.cpp)

source_group ("Header Files" FILES ${BJN_HEADERS})
source_group ("Source Files" FILES ${BJN_SOURCES})



add_library (${BJN_PROJECT} ${BJN_HEADERS} ${BJN_SOURCES})

target_link_libraries (${BJN_PROJECT} ${ZLIB_LIBRARIES})