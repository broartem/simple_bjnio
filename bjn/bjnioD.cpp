#include "bjn.h"

int	WriteBjnGzippedScalar8RecFuncByBlockD(
								  LPCHAR target,
								  LPCHAR funcName, 
								  double *f,
								  big_size i0, big_size j0, big_size k0,
								  big_size in, big_size jn, big_size kn,
								  int nBitsReduce)
{
 int err,i,j,k,n;
 LPLPLPFLOAT g;

 g=alloc_float_mas_n1_n2_n3(in,jn,kn);

 for(n=0,k=0;k<(int)kn;k++)
	 for(j=0;j<(int)jn;j++)
		 for(i=0;i<(int)in;i++)
			 g[i][j][k]=(float)f[n++];

 err=WriteBjnGzippedScalar8RecFuncByBlock(target,funcName,g,i0,j0,k0,in,jn,kn,nBitsReduce);

 free_mas_n1_n2_n3((LPLPLPVOID *)&g,in,jn,kn);

 return err;
}

int	 ReadBjnGzippedScalar8RecFuncByBlockD(
								  LPCHAR source, 
								  LPCHAR funcName, 
								  double *f, // ���������� ������ ������� � ������������� ����� 
								  big_size i0g, big_size j0g, big_size k0g,
								  big_size in, big_size jn, big_size kn)
{
 int err,i,j,k,n;
 LPLPLPFLOAT g;

 err=ReadBjnGzippedScalar8RecFuncByBlock(
								   source, funcName, &g,
								   i0g,  j0g,  k0g, in,  jn,  kn);

 if(err)
	 return err;

 for(n=0,k=0;k<(int)kn;k++)
	 for(j=0;j<(int)jn;j++)
		 for(i=0;i<(int)in;i++)
			 f[n++]=(double)g[i][j][k];

 free_mas_n1_n2_n3((LPLPLPVOID *)&g,in,jn,kn);

 return 0;
}


