#include "bjn.h"


 int update_func_block_NEXT_FUNC_OFFSET(FILE *fout, file_offset pbase, file_offset value)
 {
 int ret;
 file_seek(fout,pbase,SEEK_SET);
 ret=put_file_offset(fout,value); // ������� ��������� �� ��������� ���� �������� �������
 return ret;
 }

 int update_func_block_DATA_BLOCK_OFFSET(FILE *fout, file_offset pbase, file_offset value)
 {
 int ret;
 file_seek(fout,pbase+1*sizeof_file_offset,SEEK_SET);
 ret=put_file_offset(fout,value); // ������� ��������� �� ���� ������
 return ret;
 }

 int get_func_block_DATA_BLOCK_OFFSET(FILE *fout, file_offset pbase, file_offset *value)
 {
 int ret;
 file_seek(fout,pbase+1*sizeof_file_offset,SEEK_SET);
 ret=get_file_offset(fout,value); // ������� ��������� �� ���� ������
 return ret;
 }

 int update_func_block_FMIN(FILE *fout, file_offset pbase, float value)
 {
 int ret;
 file_seek(fout,pbase+2*sizeof_file_offset,SEEK_SET);
 ret=put_float(fout,value); // ������� fmin
 return ret;
 }

 int update_func_block_FMAX(FILE *fout, file_offset pbase, float value)
 {
 int ret;
 file_seek(fout,pbase+2*sizeof_file_offset+sizeof_float,SEEK_SET);
 ret=put_float(fout,value); // ������� fmax
 return ret;
 }

 int update_func_name(FILE *fout, file_offset pbase, LPCHAR name)
 {
	 // !! ��������, ����� �������� ��� �������������� ������ �����, �� ������ ������ 
	 // update, ���� ��� �������� �����-�� ������ - ���� ������ ����� ������, ��� 
	 // pbase+pbase+2*sizeof_file_offset+2*sizeof_float
 int ret;
 file_offset fsize;
 fsize=filelen(fout);

 if(fsize>pbase+(file_offset)(2*sizeof_file_offset+2*sizeof_float))
 {
	 printf("update_func_name: impossible of changing the func name: pbase=%u %s (some next blocks has been write already)\n",pbase,name); 
	 return 1;
 }

 file_seek(fout,pbase+2*sizeof_file_offset+2*sizeof_float,SEEK_SET);
 ret=put_string(fout,name); // ������� ��� �������
 return ret;
 }

 //==================================

int update_datablock_DATA_BLOCK_OFFSET(FILE *fout, file_offset pbase, file_offset value)
 {
 int ret;
 file_seek(fout,pbase,SEEK_SET);
 ret=put_file_offset(fout,value); // ������� ��������� �� ���� ������
 return ret;
 }

int get_data_block_DATA_BLOCK_OFFSET(FILE *fout, file_offset pbase, file_offset *value)
 {
 if(file_seek(fout,pbase,SEEK_SET))return 193;
 if(get_file_offset(fout,value))return 192; // ������ ��������� �� ���� ������
 return 0;
 }

int get_data_block_BORDERS(FILE *fout, file_offset pbase, 
						   big_size *i0, big_size *j0, big_size *k0,	
						   big_size *in, big_size *jn, big_size *kn)
 {
 if(file_seek(fout,pbase+sizeof_file_offset,SEEK_SET))return 169;
								// ������ �������� ������� ��������
 if(get_big_size(fout,i0))return 173; 
 if(get_big_size(fout,j0))return 174; 
 if(get_big_size(fout,k0))return 175; 
 if(get_big_size(fout,in))return 176; 
 if(get_big_size(fout,jn))return 177; 
 if(get_big_size(fout,kn))return 178; 
 return 0;
 }

int get_N1N2N3(FILE *fout, int *in, int *jn, int *kn)
 {
 if(file_seek(fout,LLABEL+sizeof_file_offset,SEEK_SET))return 167;
 if(get_int(fout,in))return 179; 
 if(get_int(fout,jn))return 180; 
 if(get_int(fout,kn))return 181; 
 return 0;
 }

int get_data_block_BIT_REDUCED(FILE *fout, file_offset pbase, int * nBitsReduce)
 {
 if(file_seek(fout,pbase+sizeof_file_offset+6*sizeof_big_size,SEEK_SET))return 182;
 if(get_int(fout,nBitsReduce))return 183; 
 return 0;
 }

int update_data_block_BORDERS(FILE *fout, file_offset pbase, 
						   big_size i0, big_size j0, big_size k0,	
						   big_size in, big_size jn, big_size kn,
						   int nBitsReduce)
 {
 if(file_seek(fout,pbase+sizeof_file_offset,SEEK_SET))return 172;
								
 if(put_big_size(fout,i0))return 184; 
 if(put_big_size(fout,j0))return 185; 
 if(put_big_size(fout,k0))return 186; 
 if(put_big_size(fout,in))return 187; 
 if(put_big_size(fout,jn))return 188; 
 if(put_big_size(fout,kn))return 190; 
 if(put_long(fout,nBitsReduce))return 191; 
 return 0;
 }

