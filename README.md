# Simple example of bjn file io

## Installation

From git:

	cd /working/directory/path
	git clone https://github.com/broartem/simple_bjnio.git

## Running

By MPI:

    CXX="mpicxx" CC="mpicc" cmake ./
    make
    ./bjnio_example