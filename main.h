#include "stdio.h"
#include <iostream>
#include <string>

#ifdef _WIN32
#include "bjn\bjnio.h"
#include "bjn\Zlibfiles\zlib.h"
#include "bjn\Zlibfiles\zconf.h"

#else
#include "bjn/bjnio.h"
#include "bjn/Zlibfiles/zlib.h"
#include "bjn/Zlibfiles/zconf.h"

#endif

using std::endl;
using std::cout;